function getTimed(f){
    return function(...args){
        let before = Date.now()
        f(...args)
        console.log(`${f.name} executed in ${Date.now() - before} miliseconds`)
    }
}

let f = (a,b,c) =>{
    for(let i = 0;i<1000;i++)
    console.log(a+b+c)
}

let timedF = getTimed(f)
timedF(1,2,3)