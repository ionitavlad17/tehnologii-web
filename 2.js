class Robot{
    constructor(name){
        this.name = name
    }
    move(){
        console.log(`${this.name} is moving`)
    }
}

let r0 = new Robot('some robot')
r0.move()

class Weapon{
    constructor(description){
        this.description = description
    }
    fire(){
        console.log(`firing ${this.description}`)

    }



}


let w0 = new Weapon('pew pew laser')
w0.fire()

class CombatRobot extends Robot{
    constructor(name){
        super(name)
        this.weapons = []
    }

    addWeapon(w){
        this.weapons.push(w)
    }
    fire(){
        for(let w of this.weapons){
            w.fire()
        }
    }

}

// Robot.prototype.apply(this,['some name'])
//Robot.prototype.call(this, 'some name')
//Robot.prototype.bind(this)

let r1 = new CombatRobot('a combat robot')
r1.addWeapon(w0)
r1.move()
r1.fire()


let f0 = r1.fire
//f0()
f0.apply(r1)

Robot.prototype.fly = function () {
    console.log(`${this.name} now has the ability to fly`)
}

r1.fly()

