function g(base){
    return function(n){
        base += n
        console.log(base)

    }

}

let f0 = g(10)

let f1 = g(1000)

f0(1)
f0(4)
f1(1)
f1(2)
f0(10)

