"use strict"

const express = require('express')
const request = require('request-promise')
const proxyRequest = request.defaults({
proxy:'http://cache.ase.ro:8080'
})
const app = express()
app.locals.cache = null

app.get('/weather', async(req,res)=>{
    //GET http://localhost:8080/weather?(city)
try {
    if(!app.locals.cache){
        let response = await proxyRequest('http://www.meteoromania.ro/wp-json/meteoapi/v2/starea-vremii')
        app.locals.cache = JSON.parse(response)
    }
    let city = req.query.city
    let weatherRecord = app.locals.cache.features.find((e) => e.properties.nume ===city.toUpperCase())
    if(weatherRecord)
    {
        res.status(200).json(weatherRecord)
    }
    else {
        res.status(404).json({message: "not found, try"})
    }


} catch (error) {
    console.warn(error)
    res.status(500).json({message:":("})
}
})
app.listen(8080)
