function genCheckPrime(){
    let cache = [2,3]

    let checkAgainstCache = (n) =>{
        for(let e of cache){
            if(!(n%e)){
                return false
            }
        }
        return true
    }

    return function(n){
        let found = cache.indexOf(n) !== -1

        if(found){
            return true
       }
       else{
           if(n < cache[cache.length - 1]){
               return false
           }
           else {
            for(let i = cache[cache.length-1] + 2; i<= Math.sqrt(n); i += 2){
                if( checkAgainstCache(i)){
                    console.log('ADDINg' + i)
                    cache.push(i)
                }
            }
            console.log('Cache Changed')
            console.log(cache)
            return checkAgainstCache(n)
           }
           
       }
       
    } 

}

let checkPrime = genCheckPrime()
console.log(checkPrime(121))
console.log(checkPrime(27))
console.log(checkPrime(23))
console.log(checkPrime(50))
console.log(checkPrime(17))


