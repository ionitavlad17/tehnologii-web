'use strict'

function Cat(name,color){
    this.name = name
    this.color = color
    
    this.meow = () => {
        console.log(`${this.name} is meowing`)

    }

}

let c0 = new Cat('tommy', 'Black')
c0.meow()

Cat.prototype.run = function(){
    console.log(`${this.name} is running`)


}

c0.run()